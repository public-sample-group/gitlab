---
redirect_to: '../../../topics/git/tags.md'
---

This document was moved to [another location](../../../topics/git/tags.md).

<!-- This redirect file can be deleted after <YYYY-MM-DD>. -->
<!-- Before deletion, see: https://docs.gitlab.com/ee/development/documentation/#move-or-rename-a-page -->
